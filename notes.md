Acknowledgements
----------------
This codebase is highly inspired by:
* [Building Your Own Plugin Framework - Gigi Sayfan](http://www.drdobbs.com/cpp/building-your-own-plugin-framework-part/204202899)
* [Creating a Plugin System with C++ - Claus Witt](https://www.clauswitt.com/creating-a-plugin-system-with-cpp.html)

I'm still working on it and simplifying the way it works. I don't need bells
and whistles but I want it to work in a certain way.

<code>PluginManager</code>
==============
The <code>PluginManager</code> plays host to libraries that expose functionality
in the form of objects and services.

the plugin manager is just a coordinator, such that all functionality is provided
by the plugins themselves except for a few key services.

built-in services
-----------------
* <code>registerLibrary</code>
* <code>registerService</code>
* <code>registerObject</code>
* <code>createObject</code>

Each of these services can be called with the <code>invokeService</code>
function which provides the entry point to all functionality added to the system.

<code>static int32_t PluginManager::invokeService( const char *serviceName, void *serviceStruct );</code>

The <code>PluginManager</code> has no objects, and no libraries on instantiation
Your primary application can either register objects and services directly, or
it can register a library, and the library will register objects and services.

Library
=============
A library, is the .so file, or in windows parlance .dll file. in order
for it to be considered a plugin by <code>PluginManager</code> it needs to have
these three functions specified

* <code>extern "C" PM_LibRegInfo registerFunc( const PM_HostInfo *hostInfo)</code>
* <code>int32_t initialiseFunc( const PM_HostInfo *hostInfo)</code>
* <code>int32_t finaliseFunc( const PM_HostInfo *hostInfo)</code>

The reason for the <code>extern "C"</code> is so that the symbols will not be
mangled by the c++ compiler, which makes retrieving them at runtime easier. 

During the <code>registerFunc</code>'s execution it will register services and
objects that other plugins and the primary application can use.

Service
=============
The primary method for a child plugin to interact with its host application or
host plugin.

Named functions to be called with pre-defined parameter structs, 

Object
============
The primary method for the host application, or parent plugins to interact with
a child plugin.

Pure virtual classes defined in host application orhost plugins implemented by
child plugin objects to provide the wanted functionality

walkthrough
-------
Your application creates a <code>PluginManager</code> object via the
<code>getInstance</code> method.

<code>PluginManager</code> is a singleton, its constructors are private, the
<code>getInstance</code> returns a reference to the newly created or existing
<code>PluginManager</code> object. If its the first time being run, then the 
<code>PluginManager</code> registers its own inbuilt services.

Your application can then register any services or objects it has inbuilt.

Your application can also invoke the <code>registerLibrary</code>
service with the filename of any plugin .so files you might want.

The <code>registerLibrary</code> service will run the <code>registerFunc</code>
of the library .so and keep the returned <code>PM_LibRegInfo</code> struct for
use later.

the <code>registerFunc</code> function can then register additional libraries,
objects, and services.

Your application can then invoke any services registered, or 
<code>createObject</code> any objects in the registry, to extend or replace its
own functionality


notes for self
--------------
inbuilt application code is always in memory
I want to provide these options to load plugin code:
* always - stay in memory
* on demand - get loaded when needed and removed when not
* conditional - have a trigger to load and unload.

so functions that i would expect to exist inside the .so are:
* registration - registers services and objects, .so can be safely unloaded 
                 after this stage, no internal state is set.
* initialise   - sets the internal state of the plugin, ie with factory functions
                 for singletons, global variables, cached images, data, etc
* exit         - cleans up after itself.

the registration step of each object or service also would define some of the
persistent state of the .so code. ie a function or object would demand that the
.so stay in memory. in which case the initialise function would be called.

in this way, the primary application has a cache of provided functionality of
the plugins without it needing to keep them and their dependencies in memory.

Considering that polymorphism works for known objects, simple policy for deleting
plugin objects should mitigate the requirement for 'destroyObject' facilities.
except in the case of cleanup where the parent application does not know about
the specifics of its plugin objects. but maybe that should't happen anyway.

Notes on logging style
----------------------
due to my nature of a shitty programmer, i want to define a methodology for
logging information to the stdout, and i just came up with this concept.

* **forward declaring INFO** - the act of spitting out what you are about to do
usually when about to enter a new scope
* **reverse erroring ERROR** - the act of reporting if what you have attempted has
 failed. usually after exiting a scope, for a failing but not crashing program
 it would provide almost a stack trace of commands leading up to the last
 successful action.
* what I dont want to do **reverse confirming** - the act of saying something succeeded, like no shit.
* and of course forward erroring doesnt make any sense

the above can have varying severity, for instance **FATAL**, **DEBUG** etc. but
the scemantics of when to push to stdout are there. 

even after writing this out i see a lot of shitty redundancy, i would like to know
how the greats do it.

I also want my logger to indent to the depth of the current scope, that way i
can see the flow. sort of like a call graph.
perhaps a profiler with a callgraph is actually what i want.

maybe it has merit though as requesting a depth of logging might be more useful
ie, log depth deeper and deeper to get more detailed information.

like a self destroying object that effects a singleton log class, that adds all
the additional information i want. i just insert it into the top of every function
and it gathers timestamps and statistics etc.. i'm sure this would exist already

something i can change a single define which would exclude it from compilation
once i no longer want it.. or parts of it. or whatever.


brainstorm
---------------
* it might be nice to have a veto list
* it might be nice to have unloading plugins
* it would be nice to be able to register override services
* calling a service is not guaranteed, as it can be removed
* that would necessitate
  * checkService( char *serviceName )   - test to see if it exists
  * registerService( PM_SvcRegInfo )    - non clobbering registration
  * removeService( char *serviceName )  - removing the service from the system
  * overrideRegisterService             - can be achieved with the above three
  * getServiceFunc( char *serviceName ) - returns a pointer to the service function
* and again for objects
  * checkObject( char *objectName )     - test to see if it exists
  * registerObject( PM_ObjRegInfo )     - non clobbering registration
  * removeObject( char *objectName )    - removing the object from the system
  * overrideRegisterObject              - can be achieved with the above three
  * getObjectFunc( char *objectName )   - returns a pointer to the create function
* wrapping all plugin functionality such that a crash in a plugin will just unload
    that plugin and not kill the main app.
* TODO unregister libs, services, objects
* TODO register libs, services, objects
* TODO create plugin object by name
* TODO create many plugin objects by type
* TODO split plugins into their own source directory trees
* main should detect what plugins to use and optionally create them

brainstormed plugins
--------------------
* plugin for an interactive cmd line menu
* cmd line help plugin which can be extended by further plugins

objection
---------
reading about plugins online makes me think its kind of a lost cause impossible
but then i think about the fact that we are all using shared libraries all the
time. and I come to the conclusion that i dont want plugins for cross compiled
bla blah whatever, i expect plugins built for the system to be compiled with
the same toolchain. my use case is simply optional shared libs, as needed,
rather than always loaded.
