#include <iostream>
#include "help.h"
#include "../include/argh.h"

uint32_t cmdl( int argc, const char *argv[] )
{
    argh::parser cmdl( argc, argv );
    if( cmdl[{"-h", "--help"}] ){
        std::cout << "help: Help text\n";
    }
    return 0;
}

//library registration
//====================
int32_t
initialiseFunc( const PM_HostInfo *hostInfo )
{
    if( hostInfo->invokeService( "registerOptFunc", reinterpret_cast< void *>(cmdl) ) ) return -1;
    return 0;
}

int32_t
finaliseFunc( const PM_HostInfo *hostInfo )
{
    return 0;
}

extern "C"
PM_LibRegInfo
registerFunc( const PM_HostInfo *hostInfo )
{
    PM_LibRegInfo libInfo = {
        "Help",
        API_VERSION,
        HOSTNAME,
        API_VERSION,
        initialiseFunc,
        finaliseFunc
    };
    return libInfo;
}
