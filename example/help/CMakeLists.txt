
include_directories( ${PROJECT_SOURCE_DIR}/src )

add_library( p_help SHARED
    help.h
    help.cpp
    )

set_target_properties( p_help PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/plugins
)
