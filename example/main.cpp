#include <iostream>
#include <cstring>
#include <vector>
#include <experimental/filesystem>
#include "../src/plugin_manager.h"
#include "../include/argh.h"

#include "interface.h"
#include "services.h"

namespace fs = std::experimental::filesystem;

//GLOBAL Bullshit
std::vector< cmdlAction > cmdlRegistry;

int32_t
registerCmdl( void *params )
{
    auto regCmd = reinterpret_cast< cmdlAction >( params );
    cmdlRegistry.push_back( *regCmd );
    return 0;
}

void
cmdlProcess( const int argc, const char **argv )
{
    for( auto i : cmdlRegistry )i( argc, argv );
}

uint32_t cmdl( int argc, const char *argv[] )
{
    argh::parser cmdl( argc, argv );
    if( cmdl[{"-h", "--help"}] ){
        std::cout <<
"Usage: \n"
"   -h, --help  - display this text\n\n";
    }

    if( cmdl[{"-r", "--report"}] ){
        auto &pm = PluginManager::getInstance();
        std::cout << pm.report();
    }
    return 0;
}

int
main( int argc, const char *argv[] )
{
    // Initialization
    PluginManager &pm = PluginManager::getInstance();
    pm.setHostName( HOSTNAME );

    PM_SvcRegInfo cmdlSvc = {
        "registerOptFunc",
        API_VERSION,
        API_VERSION,
        registerCmdl
    };
    pm.invokeService( "registerService", &cmdlSvc );
    pm.invokeService( "registerOptFunc", reinterpret_cast< void * >(cmdl) );

    //check path for correctness
    fs::path pluginDir( "./plugins" );
    if( !fs::exists( pluginDir ) || !fs::is_directory( pluginDir ) ){
        std::cout << "invalid path provided.\n";
        return -1;
    }
    // load the plugins
    for( auto &dirEntry : fs::directory_iterator( pluginDir ) ){
        // Skip directories
        if( fs::is_directory( dirEntry ) )continue;

        fs::path p = dirEntry.path();
        auto res = pm.invokeService( "registerLibrary", &p );
        if(res) std::cout << "Registering: " << p << " - failed\n";
    }

    cmdlProcess( argc, argv );

    //application will automatically clean up when
    //PluginManager object pm goes out of scope
    return 0;
}

