#ifndef EXAMPLE_INTERFACE_H
#define EXAMPLE_INTERFACE_H
#include <vector>
#include <string>

#include "../src/iplugin.h"

//Declare the interface for your plugins here
#define HOSTNAME "pm_example"

typedef uint32_t (*cmdlAction)( int argc, const char *argv[] );

#endif //EXAMPLE_INTERFACE_H
