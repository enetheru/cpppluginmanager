
#ifndef PLUGIN_MANAGER_H
#define PLUGIN_MANAGER_H

#include <vector>
#include <map>
#include <memory>
#include <experimental/filesystem>

#include "iplugin.h"
#include "shared_library.h"

namespace fs = std::experimental::filesystem;

class PluginManager {
public:
    static PluginManager &getInstance();
    static int32_t invokeService( const char *service, void *params );

    //services provided by default
    static int32_t    createObject( void *params );
    static int32_t  registerObject( void *params );
    static int32_t registerService( void *params );
    static int32_t    checkService( void *params );
    static int32_t registerLibrary( void *params );

    std::string report();
    //TODO change report to take some kind of flags so that it can alter the
    // report to show some or all of the information by selection. removing
    // the requirement for a version function.
    std::string version( const PM_Version & );
    std::string version() { return version( hostInfo.version ); }
    void     setVersion( const PM_Version & );

    std::string hostName();
    void     setHostName( const std::string & );

    int32_t shutdown();

private:
    PluginManager() = default;
    ~PluginManager();
    PluginManager( const PluginManager & );

public:
    const PM_HostInfo &hostInfo = hostInfo_;

private:
    PM_HostInfo hostInfo_ = {
        "rename me",
        API_VERSION,
        invokeService
    };
    std::vector< std::shared_ptr< SharedLibrary > > sharedObjects;
    std::vector< PM_LibRegInfo > libraryRegistry;
    std::vector< PM_ObjRegInfo > objectRegistry;
    std::vector< PM_SvcRegInfo > serviceRegistry;
};

#endif
