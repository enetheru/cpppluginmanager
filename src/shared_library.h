#ifndef SHARED_OBJECT_H
#define SHARED_OBJECT_H

#include <string>
#include <memory>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

class SharedLibrary
{
public:
    static SharedLibrary *load( const fs::path &path );
    void *getSymbol( const std::string &name );
    const fs::path &getPath();

    ~SharedLibrary();

private:
    explicit SharedLibrary( const fs::path &, void * );
    SharedLibrary() = default;
    fs::path path;
    void * handle = nullptr;
};

#endif //SHARED_OBJECT_H
