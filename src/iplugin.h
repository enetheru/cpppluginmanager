#ifndef PLUGIN_INTERFACE_H
#define PLUGIN_INTERFACE_H

#define API_VERSION { 0, 1, 0 }
#define PO_TYPE_MAX 256
#define PO_NAME_MAX 256
#define PO_DESC_MAX 256
#define PO_AUTH_MAX 256
#define PO_SURL_MAX 2000
#define PO_CTGS_MAX 256

extern "C" {

#include <stdint.h>

//pre declaring cyclic dependencies
typedef struct PM_LibRegInfo PM_LibRegInfo;

// Common to all
typedef struct PM_Version {
    int32_t major;
    int32_t minor;
    int32_t patch;
} PM_Version;

/* Host Platform Information and Services
 * ======================================
 * these objects are used to describe the host application and what services
 * and sockets it will make available to plugins
 */
typedef int32_t (*PM_InvokeFunc)( const char *serviceName, void *serviceParams );

typedef struct PM_HostInfo {
    char name[ PO_NAME_MAX ];
    PM_Version version;
    PM_InvokeFunc invokeService;
} PM_HostInfo;


/* SharedLibrary interface
 * ======================
 * this is the basic interface for all the .so files to determine their
 * dependencies and loading/unloading functions
 */
// macro to help building libs
#define PM_LIBRARYBOILERPLATE \
extern "C" PM_LibRegInfo registerFunc  ( const PM_HostInfo * ); \
int32_t initialiseFunc( const PM_HostInfo * ); \
int32_t finaliseFunc  ( const PM_HostInfo * );

typedef       int32_t (*PM_FinaliseFunc  )( const PM_HostInfo * );
typedef       int32_t (*PM_InitialiseFunc)( const PM_HostInfo * );
typedef PM_LibRegInfo (*PM_RegisterFunc  )( const PM_HostInfo * );

typedef struct PM_LibRegInfo {
    char fileName[ PO_NAME_MAX ];
    PM_Version version;
    char hostName[ PO_NAME_MAX ];
    PM_Version api_version;
    PM_InitialiseFunc initFunc;
    PM_FinaliseFunc finalFunc;
} PM_LibRegInfo;

/* Plugin Object Interface
 * =======================
 */
//helper when defining classes
#define PM_OBJECTBOILERPLATE \
    static    void *create( const PM_HostInfo &hostInfo );

typedef void *(*PM_CreateFunc)( const PM_HostInfo & );

typedef struct PM_ObjRegInfo {
    char name[ PO_NAME_MAX ];   //required
    char type[ PO_TYPE_MAX ];   //required
    char auth[ PO_AUTH_MAX ];
    char surl[ PO_SURL_MAX ];
    char desc[ PO_DESC_MAX ];
    char ctgs[ PO_CTGS_MAX ];
    PM_Version version;         //required
    PM_Version api_version;     //required
    PM_CreateFunc createFunc;   //required
} PM_ObjRegInfo;

/*
 * Service functions
 * =================
 * the idea behind the service func is that you can register a function that
 * you can call with arbitrary data;
 */
typedef int32_t (*PM_ServiceFunc)( void *serviceParams );

typedef struct PM_SvcRegInfo {
    char name[ PO_NAME_MAX ];
    PM_Version version;
    PM_Version api_version;
    PM_ServiceFunc serviceFunc;
} PM_SvcRegInfo;

/* Inbuilt Services Paremeter structs
 * ==================================
 * */
//Create Object Service Parameters
typedef struct SP_createObject{
    const char *objectName;
    void *object;
}SP_createObject;

}

#endif /* PLUGIN_INTERFACE_H */

