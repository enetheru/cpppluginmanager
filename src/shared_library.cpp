#include <dlfcn.h>
#include <iostream>

#include "shared_library.h"

SharedLibrary *
SharedLibrary::load( const fs::path &path )
{
    void * handle = ::dlopen( path.c_str(), RTLD_NOW );
    if( handle )return new SharedLibrary(path, handle);

    std::cout << "Failed to load " << path << ": " << std::string( ::dlerror() ) << std::endl;
    return nullptr;
}

void *
SharedLibrary::getSymbol( const std::string &symbol )
{
    return ::dlsym( handle, symbol.c_str() );
}

const fs::path &
SharedLibrary::getPath()
{
    return path;
}

SharedLibrary::SharedLibrary( const fs::path &path, void *handle = nullptr)
        : handle( handle ), path( path )
{ }

SharedLibrary::~SharedLibrary()
{
    if( handle )::dlclose( handle );
}


