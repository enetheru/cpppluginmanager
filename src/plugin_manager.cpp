#include <string>
#include <sstream>
#include <memory>
#include <iostream>
#include <cstring>

#include "plugin_manager.h"

PluginManager &
PluginManager::getInstance()
{
    static PluginManager pm;
    static bool init = false;

    if(! init ){
        init = true;
        PM_SvcRegInfo co = {
                "createObject",
                API_VERSION,
                API_VERSION,
                PluginManager::createObject
        }; pm.registerService( &co );

        PM_SvcRegInfo ro = {
                "registerObject",
                API_VERSION,
                API_VERSION,
                PluginManager::registerObject
        }; pm.registerService( &ro );

        PM_SvcRegInfo rs = {
                "registerService",
                API_VERSION,
                API_VERSION,
                PluginManager::registerService
        }; pm.registerService( &rs );

        PM_SvcRegInfo cs = {
                "checkService",
                API_VERSION,
                API_VERSION,
                PluginManager::checkService
        }; pm.registerService( &cs );

        PM_SvcRegInfo rl = {
                "registerLibrary",
                API_VERSION,
                API_VERSION,
                PluginManager::registerLibrary
        }; pm.registerService( &rl );
    }
    return pm;
}

int32_t
PluginManager::registerObject( void * params )
{
    auto objectInfo = reinterpret_cast< PM_ObjRegInfo *>( params );
    // Check parameters
    // The registration objectInfo may be received from an external plugin so it is
    // crucial to validate it, because it was never subjected to our tests.
    if(    !objectInfo
        || !::strlen( objectInfo->name )
        || !objectInfo->createFunc )return -1;

    PluginManager &pm = PluginManager::getInstance();

    // Verify that versions match
    if( pm.hostInfo.version.major != objectInfo->api_version.major )return -1;

    //TODO i'm not sure that the requirement that there can be only one is valid.
    // If item already exists in exactMatch: fail (there can be only one)
    for( auto i : pm.objectRegistry ){
        if(! ::strcmp( i.name, objectInfo->name) ) return -1;
    }

    pm.objectRegistry.push_back( *objectInfo );
    return 0;
}

int32_t
PluginManager::registerService( void * params )
{
    auto svcInfo = reinterpret_cast< PM_SvcRegInfo *>( params );

    // Check parameters
    // The registration svcInfo may be received from an external plugin so it is
    // crucial to validate it, because it was never subjected to our tests.
    if(    !svcInfo
        || !::strlen( svcInfo->name )
        || !svcInfo->serviceFunc )return -1;

    PluginManager &pm = PluginManager::getInstance();

    // Verify that versions match
    if( pm.hostInfo.version.major != svcInfo->api_version.major )return -1;

    //TODO i'm not sure that the requirement that there can be only one is valid.
    // If item already exists in exactMatch: fail (there can be only one)
    for( auto i : pm.serviceRegistry ){
        if(! ::strcmp( i.name, svcInfo->name) ) return -1;
    }

    pm.serviceRegistry.push_back( *svcInfo );
    return 0;
}

int32_t
PluginManager::checkService( void *params )
{
    //TODO cast params into char *serviceName params are the name of the service to check
    // find servicename in registry, and return 0 on success, -1 on fail
    return 0;
}

int32_t
PluginManager::invokeService( const char *service, void *params )
{
    auto &pm = getInstance();
    for( auto &reg : pm.serviceRegistry ){
        if(! ::strcmp( service, reg.name) ){
            return reg.serviceFunc( params );
        }
    }
    std::cout << "Service not found.\n";
    return -1;
};

int32_t
PluginManager::createObject( void *data )
{
    auto params = static_cast<SP_createObject *>(data);
    auto &pm = PluginManager::getInstance();

    params->object = nullptr;
    for( auto p : pm.objectRegistry ){
        if(! ::strcmp(params->objectName, p.name ) ){
            params->object = p.createFunc( pm.hostInfo );
            break; // only create one object
        }
    }

    if( params->object ) return 0;
    //else
    std::cout << "cannot create '" << params->objectName << "'\n";
    return -1;
}

int32_t
PluginManager::registerLibrary( void *p )
{
    //translate parameters into real data
    auto pluginPath = *(static_cast< fs::path *>( p ));

    //entry must exist
    if( !fs::exists( pluginPath ) ){
        std::cout << pluginPath << " does not exist\n";
        return -1;
    }

    // Skip files with the wrong extension
    if( pluginPath.extension().compare(".so") ){
        std::cout << pluginPath << " is not a shared object\n";
        return -1;
    }

    // Resolve symbolic links
    if( fs::exists( pluginPath ) && fs::is_symlink( pluginPath ) )
        pluginPath = fs::read_symlink( pluginPath );

    // Make the path absolute
    pluginPath = fs::absolute( pluginPath );

    auto &pm = PluginManager::getInstance();
    // Don't load the same dynamic library twice
    for( const auto &i : pm.sharedObjects ){
        if( i->getPath() == pluginPath ){
            std::cout << "cannot register " << pluginPath << " twice.\n";
            return -1;
        }
    }

    //so far so good, lets load up the shared library
    std::shared_ptr< SharedLibrary > so( SharedLibrary::load( pluginPath ) );
    if( !so ) {
        std::cout << pluginPath << " is not a valid shared object.\n";
        return -1;
    }// not a dynamic library?

    // Get the entry point to the shared object
    auto regFunc   = reinterpret_cast< PM_RegisterFunc >( so->getSymbol( "registerFunc" ) );
    //Note: what if the symbol exists but its not something we can actually use?
    //well its just upto the parent application not to hand it potentially bogus
    //files, its impossible to check the validity 100%, I could obfuscate it with
    //macro's creating symbols that inherently hold information ie
    //      extern "C" int PM_I_am_a_plugin_v_1_2_3() {return 0;}
    //and check that the above symbol exists before continuing on. but it ends
    //up being just a more convoluted version of what we have already checking
    //for 'registerFunc' there is always an inherent level of trust involved.
    if( !regFunc ){
        std::cout << pluginPath.filename() << " missing symbol 'registerFunc'\n";
        so.reset(); // clear the shared object, we don't want to keep it loaded.
        return -1;
    }

    //get the library registration information and check it
    bool fail = false;
    auto libInfo = regFunc( &pm.hostInfo );
    if( ::strcmp( libInfo.hostName, pm.hostInfo.name ) ){
        fail = true;
        std::cout << "Hostname mismatch PM: " << pm.hostInfo.name << ", Plugin: "
                  << libInfo.hostName << "\n";
    }
    if( libInfo.version.major != pm.hostInfo.version.major ) {
        fail = true;
        std::cout << "version mismatch PM: " << pm.version() << ", Plugin: "
                  << pm.version(libInfo.api_version) << "\n";
    }
    if (!libInfo.initFunc) {
        fail = true;
        std::cout << pluginPath << " missing symbol 'initaliseFunc'\n";
    }
    if (!libInfo.finalFunc) {
        fail = true;
        std::cout << pluginPath << " missing symbol 'finaliseFunc'\n";
    }
    if( fail ){ so.reset(); return -1; }

    pm.sharedObjects.push_back( so );
    pm.libraryRegistry.push_back( libInfo );

    //FIXME for now initialise plugin now its been checked, in future this may
    //wait on an explicit call to load, and finalise it. because just because it
    //exists doesnt mean we necessarily want to use it. I just have to figure
    //out how we might not even load the .so code into memory, but i think that
    //it will require external configuration, or perhaps merging the SharedObject
    //and LibRegInfo structures into one thing so that the information to load
    //and unload the .so is in the registration.
    libInfo.initFunc( &pm.hostInfo );
    return 0;
}

std::string
PluginManager::report()
{
    std::stringstream report;
    report << "=PluginManager Report=\n"
        << "Shared Objects(.so) loaded: " << (int)sharedObjects.size() << std::endl;
    for( auto so : sharedObjects ){
        report << "\t" << so->getPath() << std::endl;
    }
    report << "Services in Registry:\n";
    for( auto reg : serviceRegistry ){
        report << "\t" << reg.name << std::endl;
    }
    report << "Objects in Registry:\n";
    for( auto reg : objectRegistry ){
        report << "\t" << reg.type << "->" << reg.name << std::endl;
    }
    return report.str();
}

std::string
PluginManager::version( const PM_Version &version )
{
    std::stringstream stream;
    stream << version.major << "." << version.minor << "." << version.patch;
    return stream.str();
}

void
PluginManager::setHostName( const std::string &name )
{
    ::strcpy( hostInfo_.name, name.c_str() );
}

void
PluginManager::setVersion( const PM_Version &version )
{
    hostInfo_.version.major = version.major;
    hostInfo_.version.minor = version.minor;
    hostInfo_.version.patch = version.patch;
}

int32_t
PluginManager::shutdown()
{
    int32_t result = 0;
    for( auto lib : libraryRegistry ) {
        try {
            result = (*lib.finalFunc)( &hostInfo );
        }
        catch (...) {
            result = -1;
        }
    }

    //dynamicLibraryMap.clear();
    sharedObjects.clear();
    objectRegistry.clear();
    libraryRegistry.clear();

    return result;
}

PluginManager::~PluginManager() {
    // Just in case it wasn't called earlier
    shutdown();
}
