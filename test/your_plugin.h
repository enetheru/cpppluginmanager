//
// Created by stream on 9/9/17.
//

#ifndef PROJECT_YOUR_PLUGIN_H
#define PROJECT_YOUR_PLUGIN_H

#include "../src/iplugin.h"
#include "interface.h"

PM_LIBRARYBOILERPLATE

class Your_Plugin_Object : public TestPlugin {
public:
    PM_OBJECTBOILERPLATE
    void plugin_function( const std::string &test );
};


#endif //PROJECT_YOUR_PLUGIN_H
