//
// Created by stream on 9/7/17.
//
#include <iostream>
#include "my_plugin.h"

// Boiler Plate for plugins
int32_t
initialiseFunc( const PM_HostInfo *hostInfo )
{
    PM_ObjRegInfo regInfo = {
            "My_Plugin_Object",
            "I_Plugin_Object",
            "Samuel Nicholas",
            "http://gitlab.com/whatever",
            "description of first test plugin interface thingo",
            "no category",
            PLUGIN_VERSION,
            API_VERSION,
            My_Plugin_Object::create
    };

    //auto res = hostInfo->registerObject( &regInfo );
    if( hostInfo->invokeService( "registerObject", &regInfo ) )
    {
        std::cout << "failed to register My_Plugin_Object\n";
        return -1;
    }

    return 0;
}

int32_t
finaliseFunc( const PM_HostInfo * )
{
    std::cout << "my_plugin.so finalising\n";
    return 0;
}


// Plugin Object implementation
void *
My_Plugin_Object::create( const PM_HostInfo &hostInfo ){
        std::cout << "My_Plugin_Object created\n";
        return new My_Plugin_Object;
}

void
My_Plugin_Object::plugin_function( const std::string &test )
{
    std::cout << "My_Plugin_Object::plugin_function( " << test << " );\n";
}

My_Plugin_Object::~My_Plugin_Object() {
    std::cout << "My_Plugin_Object::~destructor\n";
}

// shared library entry and exit points
extern "C"
PM_LibRegInfo
registerFunc( const PM_HostInfo * hostInfo )
{
    //library registration
    PM_LibRegInfo libInfo = {
            "name",
            API_VERSION,
            HOSTNAME,
            API_VERSION,
            initialiseFunc,
            finaliseFunc,
    };
    return libInfo;
}



