//
// Created by stream on 9/14/17.
//

#include <iostream>
#include "nest.h"

// Plugin Object implementation
void *
Nest_Object::create( const PM_HostInfo &hostInfo ){
    auto nest_object = new Nest_Object;

    std::cout << "Constructing Nest_Object\n";

    std::cout << "Invoking 'null' service: ";
    if(! hostInfo.invokeService( "null", nullptr ) )std::cout << "success\n";
    else std::cout << "failed\n";

    std::cout << "Invoking 'PM_createObject' service with 'Egg_Object'\n";
    SP_createObject params = { "Egg_Object", nullptr };
    if(! hostInfo.invokeService( "createObject", &params ) ){
        std::cout << "Invoking service success\n";
        nest_object->egg_object = static_cast< IEgg_Object *>( params.object );
        nest_object->egg_object->functionality();
    } else {
        std::cout << "Invoking service failed\n";
    }

    std::cout << "Invoking 'Nest_Service' Service\n";
    if(! hostInfo.invokeService( "Nest_Service", nullptr ) )
        std::cout << "Invoking service success\n";
    else
        std::cout << "Invoking service failed\n";

    return nest_object;
}

Nest_Object::~Nest_Object()
{
    delete egg_object;
}

int32_t beans( void *params )
{
    std::cout << "Whaaatt!!\n";
};

// shared library entry and exit points
int32_t
finaliseFunc( const PM_HostInfo *hostInfo )
{
    return 0;
}

int32_t
initialiseFunc( const PM_HostInfo *hostInfo )
{
    PM_ObjRegInfo objRegInfo = {
            "Nest_Object",
            "Nest_Object",
            "Samuel Nicholas",
            "http://gitlab.com/whatever",
            "description of first test plugin interface thingo",
            "no category",
            API_VERSION,
            API_VERSION,
            Nest_Object::create
    };
    auto res = hostInfo->invokeService( "registerObject", &objRegInfo );
    if( res < 0 ){
        std::cout << "Registration of Nest_Object Failed\n";
        return -1;
    }

    PM_SvcRegInfo svcRegInfo = {
            "Nest_Service",
            API_VERSION,
            API_VERSION,
            beans
    };
    res = hostInfo->invokeService( "registerService", &svcRegInfo );
    if( res < 0 ){
        std::cout << "registration of 'Nest_Service' failed\n";
        return -1;
    }

    return 0;
}

extern "C"
PM_LibRegInfo
registerFunc( const PM_HostInfo *hostInfo )
{
    std::cout << "nest.so loading\n";

    PM_LibRegInfo libRegInfo = {
            "Nest Plugin",
            API_VERSION,
            HOSTNAME,
            API_VERSION,
            initialiseFunc,
            finaliseFunc
    };

    return libRegInfo;
}

