//
// Created by stream on 9/7/17.
//

#ifndef MY_PLUGIN_H
#define MY_PLUGIN_H
#include "interface.h"

#define PLUGIN_VERSION {1,1,1}

PM_LIBRARYBOILERPLATE

class My_Plugin_Object : public TestPlugin {
public:
    PM_OBJECTBOILERPLATE

    void plugin_function( const std::string &test );
    ~My_Plugin_Object();
};


#endif //MY_PLUGIN_H
