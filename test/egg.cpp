//
// Created by stream on 9/14/17.
//

#include <iostream>
#include "egg.h"


// Plugin Object implementation
void *
Egg_Object::create( const PM_HostInfo &hostInfo ){
    std::cout << "Egg_Object created\n";
    return new Egg_Object;
}

void
Egg_Object::functionality() {
    std::cout << "Egg_Object::functionality()\n";
}

Egg_Object::~Egg_Object() {
    std::cout << "~Egg_Object()\n";
}

// shared library entry and exit points
int32_t
initialiseFunc( const PM_HostInfo *hostInfo )
{
    PM_ObjRegInfo regInfo = {
            "Egg_Object",
            "Egg_Object",
            "Samuel Nicholas",
            "http://gitlab.com/whatever",
            "description of first test plugin interface thingo",
            "no category",
            API_VERSION,
            API_VERSION,
            Egg_Object::create,
    };


    auto res = hostInfo->invokeService( "registerObject", &regInfo );
    if( res < 0 )return -1;

    return 0;
}

int32_t
finaliseFunc( const PM_HostInfo *hostInfo )
{
    return 0;
}

extern "C"
PM_LibRegInfo
registerFunc( const PM_HostInfo *info )
{
    PM_LibRegInfo libInfo = {
        "Egg Library",
        API_VERSION,
        HOSTNAME,
        API_VERSION,
        initialiseFunc,
        finaliseFunc
    };
    return libInfo;
}

