//
// Created by stream on 9/9/17.
//
#include <iostream>

#include "your_plugin.h"

// Plugin Object implementation
void *
Your_Plugin_Object::create( const PM_HostInfo &hostInfo ){
    std::cout << "Your_Plugin_Object created\n";
    return new Your_Plugin_Object;
}

void
Your_Plugin_Object::plugin_function( const std::string &test )
{
    std::cout << "Your_Plugin_Object::plugin_function( " << test << " );\n";
}

// shared library entry and exit points
int32_t
initialiseFunc( const PM_HostInfo *hostInfo )
{
    PM_ObjRegInfo regInfo = {
            "Your_Plugin_Object",
            "I_Plugin_Object2",
            "Samuel Nicholas",
            "http://gitlab.com/whatever",
            "description of second test plugin interface thingo",
            "no category",
            API_VERSION,
            API_VERSION,
            Your_Plugin_Object::create,
    };

    auto res = hostInfo->invokeService("registerObject", &regInfo );
    if( res < 0 ){
        std::cout << "Registration of Your_Plugin_Object failed.";
        return -1;
    }

    return 0;

}

int32_t
finaliseFunc( const PM_HostInfo *hostInfo )
{
    return 0;
}

extern "C"
PM_LibRegInfo
registerFunc( const PM_HostInfo *info )
{
    PM_LibRegInfo libInfo = {
        "Your Plugin",
        API_VERSION,
        HOSTNAME,
        API_VERSION,
        initialiseFunc,
        finaliseFunc
    };
    return libInfo;
}

