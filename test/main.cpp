#include <iostream>
#include <experimental/filesystem>
#include "../src/plugin_manager.h"
#include "interface.h"
#include "argh.h"

namespace fs = std::experimental::filesystem;


const char *helptext =
        "Usage: plugins2 <plugins dir>\n"
        "";

int
main( int argc, const char *argv[] )
{
    //parse command line arguments
    argh::parser cmdl( argc, argv, argh::parser::SINGLE_DASH_IS_MULTIFLAG );
    bool bail = false;
    if( argc < 2 || cmdl["h"] || cmdl["help"] ){
        std::cout << helptext;
    }
    //check path for correctness
    fs::path pluginDir = fs::path( cmdl[ 1 ].c_str() );
    if( !fs::exists( pluginDir ) || !fs::is_directory( pluginDir ) ){
        std::cout << "invalid path provided.\n";
        return -1;
    }

    // Initialization
    PluginManager &pm = PluginManager::getInstance();
    pm.setHostName( HOSTNAME );

    // load the plugins
    for( auto &dirEntry : fs::directory_iterator( pluginDir ) ){
        // Skip directories
        if( fs::is_directory( dirEntry ) )continue;

        fs::path p = dirEntry.path();
        auto res = pm.invokeService( "registerLibrary", &p );
        if(res) std::cout << "Registering: " << p << " - failed\n";
    }
    std::cout << pm.report() << std::endl;

    // FIXME This main should be completely removed into tests
    // and an examples folder should exist too

    //create some objects
    SP_createObject params = {"My_Plugin_Object", nullptr};
    if(! pm.invokeService( "createObject", &params ) ){
        auto object = static_cast<TestPlugin *>( params.object );
        object->plugin_function("beans");
        delete object;
    }
    params = {"Nest_Object", nullptr};
    if(! pm.invokeService( "createObject", &params ) ){
        delete params.object;
    }
    //application will automatically clean up when
    //PluginManager object pm goes out of scope
    return 0;
}

