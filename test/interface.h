#ifndef TEST_INTERFACE_H
#define TEST_INTERFACE_H
#include <vector>
#include <string>

#include "iplugin.h"

//Declare the interface for your plugins here
#define HOSTNAME "pm_test"

class TestPlugin {
public:
    // functionality that the parent wants to use gets defined
    // as pure virtual here:
    virtual void plugin_function( const std::string &test ) = 0;
    virtual ~TestPlugin() = default;
};

#endif //TEST_INTERFACE_H
