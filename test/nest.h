//
// Created by stream on 9/14/17.
//

#ifndef PROJECT_NESTED_BASE_H
#define PROJECT_NESTED_BASE_H

#include "../src/iplugin.h"
#include "interface.h"

PM_LIBRARYBOILERPLATE

class IEgg_Object {
public:
    virtual void functionality() = 0;
    virtual ~IEgg_Object() = default;
};

class Nest_Object {
    IEgg_Object *egg_object = nullptr;
public:
    PM_OBJECTBOILERPLATE
    ~Nest_Object();
};



#endif //PROJECT_NESTED_BASE_H
