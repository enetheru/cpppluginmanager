//
// Created by stream on 9/14/17.
//

#ifndef PROJECT_NESTED_CHILD_H
#define PROJECT_NESTED_CHILD_H

#include "nest.h"

#define PLUGIN_VERSION {1,1,1}

class Egg_Object: public IEgg_Object {
public:
    PM_OBJECTBOILERPLATE

    void functionality();
    ~Egg_Object();
};


#endif //PROJECT_NESTED_CHILD_H
